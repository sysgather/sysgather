# Various utility functions
#
# Copyright (c) 2005-2007, 2010  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# $Ringlet$

# Read the configuration file and parse it into package collections

sub readconf($)
{
	my ($argref) = @_;
	my ($fname, $grp, $g, %c) = ($conf{'conffile'});

	print "Parsing the configuration file...\n" if $verbose;
	if (!tie %c, 'Config::IniFiles',
	    (-file => $fname, -allowcontinue => 1)) {
		my $err = join "\n", "Could not read $fname",
		    @Config::IniFiles::errors;
		die "$err\n";
	}
	$cfg{$_} = $c{$_} for keys %c;

	if ($conf{'_process_home'} && defined($conf{'conflocal'}) &&
	    -e $conf{'conflocal'} && tie %c, 'Config::IniFiles',
	    (-file => $conf{'conflocal'}, -allowcontinue => 1)) {
		$cfg{$_} = $c{$_} for grep { $_ ne 'default' } keys %c;
		if (exists($c{'default'})) {
			if (exists($cfg{'default'})) {
				$cfg{'default'}{$_} = $c{'default'}{$_} for
				    keys %{$c{'default'}};
			} else {
				$cfg{'default'} = $c{'default'};
			}
		}
	}

	if (!exists($cfg{'default'})) {
		die "The $fname file does not contain a 'default' section!\n";
	}
	if (!defined($cfg{'default'}{'groups'})) {
		die "No groups specified in the default section of $fname\n";
	}
	foreach (@{$conf{'confvars'}}) {
		my $val = $cfg{'default'}{$_};
		next unless defined $val;
		if (ref $conf{$_} eq '') {
			$conf{$_} = $val;
		} elsif (ref $conf{$_} eq 'ARRAY') {
			$conf{$_} = [ split /\s+/, $val ];
		} else {
			die "Internal error: attempting to replace a ".
			    ref($conf{$_})." config variable '$_'\n";
		}
	}
	if (defined($cfg{'default'}{'mapbase'})) {
		my $base = $cfg{'default'}{'mapbase'};
		for (qw/mapconf mapsrc/) {
			my $new = $cfg{'default'}{$_};
			next unless defined($new);
			$conf{$_} = sub {
				$_[0] =~ s{^\Q${base}\E/}{${new}/}o;
				return $_[0];
			}
		}
	}
	foreach $grp (split /\s+/, $cfg{'default'}{'groups'}) {
		print "Parsing group $grp...\n" if $verbose;
		if (!exists($cfg{$grp})) {
			die "No $grp group in $fname\n";
		}
		$g = $cfg{$grp};
		foreach (@{$conf{'collvars'}}) {
			die "No $_ entry in group $grp\n" if !defined($g->{$_});
		}
		$groups{$grp} = $g;
	}
	print "Parsed ".scalar(keys %groups)." groups\n" if $verbose;
	if (defined($argref) && @{$argref} == 1 && ${$argref}[0] eq 'ALL') {
		print "Using all groups\n" if $verbose;
		@{$argref} = sort keys %groups;
		$conf{'_process_all'} = 1;
	}
}
