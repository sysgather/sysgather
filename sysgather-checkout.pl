#!/usr/bin/perl -w

# Copyright (c) 2010  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# $Ringlet$

=head1 NAME

sysgather-checkout - check out a sysgather config tree from a VCS

=head1 SYNOPSIS

sysgather-checkout [-hnqVv] [-f file] [-s section] package...

=head1 DESCRIPTION

The B<sysgather-checkout> utility checks out a B<sysgather> configuration
tree from a version control system, such as Subversion, CVS, etc.

=head1 OPTIONS

=over 4

=item B<-f> I<file>

Specify the configuration file to use instead of the default
B</usr/local/etc/sysgather.conf>.

B<Note:> If this option is present, the F<~/.sysgather.conf> file
is B<not> processed after the specified configuration file.

=item B<-h>

Display usage information and exit.

=item B<-n>

Do not actually check out any files, but output the commands that
would be executed to the standard output stream.

=item B<-q>

Quiet operation - suppress informational and warning messages, only
complain about genuine error conditions.  A no-op for the present.

=item B<-V>

Display the program version and exit.

=item B<-v>

Verbose operation - display progress messages during the program's work.

=back

=cut

use strict;
use warnings;

use Config::IniFiles;
use Getopt::Std;

sub run_command(@);
sub usage($);
sub version();

# The actual check-out routines
sub checkout_svn(@);

# The sysgather-common subroutines used
sub readconf($);

my ($quiet, $verbose) = (0, 0);

my %conf = (
	'conffile'	=> '/usr/local/etc/sysgather.conf',
	'conflocal'	=> $ENV{'HOME'}.'/.sysgather.conf',
	'noaction'	=> 0,
	'section'	=> 'sysgather-checkout',
);

my %modes = (
	'svn'		=> \&checkout_svn,
);

my %cfg = ();
my %groups = ();

# The main program - parse command-line options and execute a command
MAIN:
{
	my ($sect);
	my (@args);
	my (%opts);
	
	getopts('f:hnqvV', \%opts) or usage(1);
	# Trivial options
	version() if $opts{'V'};
	usage(0) if $opts{'h'};
	$quiet = 1 if $opts{'q'};
	$verbose = 1 if $opts{'v'};
	# And now for the real ones
	if ($opts{'f'}) {
		$conf{'conffile'} = $opts{'f'};
		$conf{'_process_home'} = 0;
	}
	$conf{'section'} = $opts{'s'} if $opts{'s'};
	$conf{'noaction'} = 1 if $opts{'n'};

	usage(1) unless (@ARGV);
	@args = @ARGV;
	readconf(\@args);

	$sect = $cfg{$conf{'section'}};
	die("Missing configuration section '$conf{section}'\n") unless $sect;
	if (!defined($sect->{'mode'})) {
		die("Missing 'mode' in the '$conf{section}' section\n");
	} elsif (!defined($modes{$sect->{'mode'}})) {
		die("Unsupported checkout mode '$sect->{mode}'\n");
	}
	&{$modes{$sect->{'mode'}}}(@args);
}

sub usage($)
{
	my ($err) = @_;
	my $s = "Usage: sysgather-checkout [-hnqvV] [-f file] package...\n".
	    "\t-f file\tspecify the config file name;\n".
	    "\t-h\tdisplay usage information and exit;\n".
	    "\t-n\ttest mode, only display what would have been done;\n".
	    "\t-q\tquiet operation, only display genuine error messages;\n".
	    "\t-v\tverbose operation;\n".
	    "\t-V\tdisplay version information and exit.\n";

	if ($err) {
		print STDERR $s;
	} else {
		print $s;
	}
	exit($err) if $err;
}

sub version()
{
	print "sysgather-checkout version 1.0\n";
}

sub checkout_svn(@)
{
	my (@args) = @_;
	my ($base, $sect);
	my (@missing, @dirs, @cmd, @c);

	# Sanity check
	foreach (@args) {
		die "Unknown package $_\n" if !defined($groups{$_});
	}
	if ($verbose) {
		if ($conf{'_process_all'}) {
			print "About to svn-checkout *everything*\n";
		} else {
			print "About to svn-checkout @args\n";
		}
	}

	$sect = $cfg{$conf{'section'}};
	foreach (qw/svn_root svn_repo/) {
		push @missing, $_ unless defined($sect->{$_});
	}
	if (@missing) {
		die("Missing @missing in '$conf{section}'\n");
	}
	$base = $sect->{'svn_root'}.$sect->{'svn_repo'};

	if ($conf{'_process_all'}) {
		@dirs = ("$base");
	} else {
		my $conf = $cfg{'default'}->{'mapbase'};
		$conf = qr{^\Q$conf\E/} if defined($conf);

		foreach (@args) {
			my $d = $groups{$_}->{'confdir'};
			$d =~ s{$conf}{} if $conf;
			push @dirs, $d;
		}
	}
	print scalar(@dirs)." directories to check out:\n".
	    join('', map "\t$_\n", @dirs) if $verbose;

	@cmd = ($sect->{'svn_program'} || 'svn', '--non-interactive',
	    ($sect->{'svn_user'}? ('--username', $sect->{'svn_user'}): ()),
	    ($sect->{'svn_pass'}? ('--password', $sect->{'svn_pass'}): ()),
	    'export', '-rHEAD');
	if ($conf{'_process_all'}) {
		run_command(@cmd, '--force', "$dirs[0]", '.');
	} else {
		foreach (@dirs) {
			run_command(@cmd, "$base/$_", $_);
		}
	}
}

sub run_command(@)
{
	my (@c) = @_;

	print "About to execute @c\n" if $verbose;
	if ($conf{'noaction'}) {
		print "@c\n";
	} else {
		system @c;
	}
}

=head1 CONFIGURATION FILE SYNTAX

The B<sysgather-checkout> utility uses the same configuration file as
B<sysgather>; see the latter's manual for the file format.

By default, B<sysgather-checkout> examines the C<sysgather-checkout>
section of the configuration file; this may be overridden by using
the B<-s> command-line option.  There is a single mandatory setting,
C<mode> - it determines the type of version control system to be used.
Currently, only Subversion is supported.

=head2 SUBVERSION CONFIGURATION

The C<svn> mode of operation of the B<sysgather-checkout> utility is
controlled by the following parameters:

=over 4

=item * svn_root

The root of the Subversion repository.

=item * svn_repo

The path within the Subversion repository.  This is the path that
corresponds to the root of the B<sysgather> tree - the one pointed to
by the C<mapconf> configuration file parameter (see the C<MAPPING DIRECTORIES>
section of the B<sysgather> manual page for more information on C<mapbase>
and C<mapconf>).

=item * svn_user

(optional) The username to use for exporting the tree; if omitted,
B<sysgather-checkout> will invoke the Subversion client without specifying
a username, so that cached credentials may be used.

=item * svn_pass

(optional) The password to use for exporting the tree; if omitted,
B<sysgather-checkout> will invoke the Subversion client without specifying
a password, so that cached credentials may be used.

=back

=head1 FILES

=over 4

=item F</usr/local/etc/sysgather.conf>

The default configuration file, unless overridden by the B<-f> command-line
option.

=item F<~/.sysgather.conf>

The per-user configuration file, located in the home directory of the
account invoking B<sysgather>.  The contents of this file is merged with
the contents of the system-wide file as described above.

=back

=head1 EXAMPLES

Perform a full checkout as defined by the C<sysgather-checkout> section
of the F</usr/local/etc/sysgather/sysgather.conf> file:

  sysgather-checkout ALL

Only check out the C<apache2> and C<sysgather> collections:

  sysgather-checkout apache2 sysgather

Specify a different configuration file and section:

  sysgather-checkout -f ../sg-local.conf -s checkout ALL

=head1 SEE ALSO

B<sysgather(1)>

=head1 BUGS

=over 4

=item *

There is no B<-O> I<option=value> command-line option.

=item *

This documentation is much too sketchy.

=item *

There is no test suite.

=back

=head1 HISTORY

The B<sysgather-checkout> utility was written by Peter Pentchev in 2010.

=head1 AUTHOR

Peter Pentchev E<lt>roam@ringlet.netE<gt>

=cut

