# Copyright (c) 2005, 2006, 2010  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# $Ringlet$

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man/man
MAN1DIR?=	${MANDIR}1
CONFDIR?=	${PREFIX}/etc
EXAMPLESDIR?=	${PREFIX}/share/examples/sysgather

SRC=		sysgather.pl sysgather-common.pl
SRC_CH=		sysgather-checkout.pl sysgather-common.pl
TESTCONF=	sysgather.conf
REALCONF=	sysgather.conf.real
TESTPKG=	sys-fbsd5 apache
#TESTCMD=	source get
TESTCMD=	source diff

BINOWN?=	root
BINGRP?=	wheel
BINMODE?=	555
SHAREOWN?=	root
SHAREGRP?=	wheel
SHAREMODE?=	444

INSTALL?=	install
COPY?=		-c
STRIP?=		-s
INSTALL_PROGRAM?=	${INSTALL} ${COPY} ${STRIP} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_SCRIPT?=	${INSTALL} ${COPY} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=	${INSTALL} ${COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

PROG=		sysgather sysgather-checkout
MANPAGE=	sysgather.1 sysgather-checkout.1

MKDIR?=		mkdir -p
RM?=		rm -f
SED?=		sed
CP?=		cp
TAR?=		tar
PODCHECKER?=	podchecker

TAR_CREATE?=	-zcf
PODCHECKER_ARGS?=	-w

VERSION?=	1.0

DISTNAME?=	${PROG}-${VERSION}
DIST_SUFFIX?=	.tar.gz
DISTARCHIVE?=	${DISTNAME}${DIST_SUFFIX}
DISTFILES?=	Makefile NEWS examples sysgather.conf sysgather.pl web

all:		${PROG} ${REALCONF} ${MANPAGE}

sysgather:	${SRC}
		set -e; for i in ${SRC}; do perl -c $$i; done
		${SED} -e "s@/usr/local/etc@${CONFDIR}@g; s@/usr/local@${PREFIX}@g; s@/usr/local/share/examples/sysgather@${EXAMPLESDIR}@g" ${SRC} > $@ || ${RM} $@
		perl -c $@

sysgather-checkout:	${SRC_CH}
		set -e; for i in ${SRC_CH}; do perl -c $$i; done
		${SED} -e "s@/usr/local/etc@${CONFDIR}@g; s@/usr/local@${PREFIX}@g; s@/usr/local/share/examples/sysgather@${EXAMPLESDIR}@g" ${SRC_CH} > $@ || ${RM} $@
		perl -c $@

sysgather.1:	sysgather
		${PODCHECKER} ${PODCHECKER_ARGS} $<
		pod2man --section=1 $< > $@ || ${RM} $@

sysgather-checkout.1:	sysgather-checkout
		${PODCHECKER} ${PODCHECKER_ARGS} $<
		pod2man --section=1 $< > $@ || ${RM} $@

${REALCONF}:	${TESTCONF}
		${SED} -e "s@/usr/local/etc@${CONFDIR}@g; s@/usr/local@${PREFIX}@g; s@/usr/local/share/examples/sysgather@${EXAMPLESDIR}@g" ${TESTCONF} > ${REALCONF} || ${RM} ${REALCONF}

install:	all
		${MKDIR} ${DESTDIR}${BINDIR}
		${INSTALL_SCRIPT} ${PROG} ${DESTDIR}${BINDIR}
		${MKDIR} ${DESTDIR}${MAN1DIR}
		${INSTALL_DATA} ${MANPAGE} ${DESTDIR}${MAN1DIR}
		${MKDIR} ${DESTDIR}${EXAMPLESDIR}
		${INSTALL_DATA} examples/*.conf ${DESTDIR}${EXAMPLESDIR}/
		${MKDIR} ${DESTDIR}${CONFDIR}
		${INSTALL_DATA} ${REALCONF} ${DESTDIR}${CONFDIR}/${TESTCONF}.default
		if [ ! -f "${DESTDIR}${CONFDIR}/${TESTCONF}" ]; then \
			${INSTALL_DATA} ${REALCONF} ${DESTDIR}${CONFDIR}/${TESTCONF}; \
		fi

mainttest:
		for i in ${TESTCMD}; do \
			echo "--- Testing $$i ${TESTPKG}"; \
			perl -w ${SRC} -vf ${TESTCONF} $$i ${TESTPKG}; \
		done

clean:
		${RM} ${PROG} ${MANPAGE} ${REALCONF}

dist:		all
		${RM} -r ${DISTNAME}/
		${MKDIR} ${DISTNAME}
		${CP} -Rp ${DISTFILES} ${DISTNAME}/
		${TAR} -zcf ${DISTARCHIVE} ${DISTNAME}/

.PHONY:		all install mainttest clean dist
